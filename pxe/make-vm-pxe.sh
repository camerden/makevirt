#!/bin/sh
set -e -u

require() {
	! while test $# -gt 0
	do
		command -v "$1" >/dev/null 2>&1 || {
			printf 'Cannot find required %s\n' "$1"
		}
		shift
	done | grep -e . >&2
}

expand_cpulist() {
	awk -F, '
	{
		for (i = 1; i <= NF; ++i) {
			if (i > 1) printf ","
			if ($i ~ "^[0-9]+-[0-9]+$") {
				split($i, range, "-")
				for (j = range[1]; j <= range[2]; ++j) {
					if (j > range[1]) printf ","
					printf "%u", j
				}
			} else
				printf "%u", $i
		}
		printf "\n"
	}'
}

get_cpu_topology() (
	is_ppc64=$(command -v ppc64_cpu >/dev/null 2>&1 && echo true || echo false)

	test -f /sys/devices/system/cpu/cpu0/topology/physical_package_id || return 1
	sockets=$(sort -u /sys/devices/system/cpu/cpu*/topology/physical_package_id | wc -l)
	test "${sockets}" -gt 0 || return 1
	printf 'sockets=%u' "${sockets}"

	$is_ppc64 \
	&& cores=$(ppc64_cpu --cores-present | grep -o '[0-9]\{1,\}$') \
	|| cores=$(
		sort -u $(
			if test -f /sys/devices/system/cpu/cpu0/topology/package_cpus
			then
				# new name
				echo /sys/devices/system/cpu/cpu*/topology/package_cpus
			elif test -f /sys/devices/system/cpu/cpu0/topology/core_siblings_list
			then
				# old name
				echo /sys/devices/system/cpu/cpu*/topology/core_siblings_list
			else
				return 1
			fi
		) \
		| expand_cpulist \
		| awk -F, '{ print NF }' \
		| awk '{
			  if (!ctr[$0]++ && NR > 1) {
				  print "Cores per package does not match across CPUs" > /dev/stderr
				  exit 1
			  }
			  print
		  }'
	)
	test "${cores}" -gt 0 || return 1
	printf ',cores=%u' "${cores}"

	$is_ppc64 \
	&& threads=$(ppc64_cpu --threads-per-core | grep -o '[0-9]\{1,\}$') \
	|| threads=$(
		sort -u $(
			if test -f /sys/devices/system/cpu/cpu0/topology/core_cpus_list
			then
				# new name
				echo /sys/devices/system/cpu/cpu*/topology/core_cpus_list
			elif test -f /sys/devices/system/cpu/cpu0/topology/thread_siblings_list
			then
				# old name
				echo /sys/devices/system/cpu/cpu*/topology/thread_siblings_list
			else
				return 1
			fi
		) \
		| expand_cpulist \
		| awk -F, '{ print NF }' \
		| sort -u \
		| awk '{
			  if (!ctr[$0]++ && NR > 1) {
				  print "Threads per core does not match across cores" > /dev/stderr
				  exit 1
			  }
			  print
		  }'
	)
	test "${threads}" -gt 0 || return 1
	printf ',threads=%u' "${threads}"

	echo
)

genmac() {
	# vendor prefix
	printf '52:54:00'
	# random suffix
	for _ in 0 1 2
	do
		printf ':%s' "$(tr -cd '0-9a-f' </dev/urandom | dd bs=1 count=2 2>/dev/null)"
	done
}

usage() {
        printf 'Usage: %s [-h] -n VM_NAME [-c VM_NTHREADS] [-m VM_MEM] [-d VM_DSKSZ] [-p VM_DSKPOOL] [-t TFTPBOOT_DIR]\n' "$0"
}

help() {
	cat <<-EOF
	$(usage)

	Options:
	  -h	prints this help message.
	  -c	number of CPU threads to assign to the VM (defaults to: ${VM_NTHREADS}).
	  -d	disk size (in GiB) of the VM (defaults to: ${VM_DSKSZ}).
	  -n	the name of VM to create and install.
	  -m	memory (in MiB) to assign to the VM (defaults to: ${VM_MEM}).
	  -p	the storage pool to use for the VM's disk.
	  -t	set the tftpboot directory (defaults to: ${TFTPBOOT_DIR}).

	EOF
}

require dnsmasq virsh virt-install

# Defaults
TFTPBOOT_DIR=/var/lib/libvirt/tftpboot
TFTP_FILE=/boot/grub/powerpc-ieee1275/core.elf
VM_NTHREADS=2
VM_MEM=2048
VM_DSKPOOL=
VM_DSKSZ=10

while getopts 'hc:d:n:m:p:t:' _opt
do
	case $_opt
	in
		(h)
			help
			exit 0
			;;
		(c)
			test "$OPTARG" -ge 1 || {
				echo 'VM must have a least one thread.' >&2
				exit 1
			}
			VM_NTHREADS=$OPTARG
			;;
		(d)
			test "$OPTARG" -gt 0 || {
				echo 'VM must have a least a disk size > 0.' >&2
				exit 1
			}
			VM_DSKSZ=$OPTARG
			;;
		(n)
			vm_name=$OPTARG
			;;
		(m)
			test "$OPTARG" -ge 64 || {
				echo 'VM must have a least 64 MiB of memory.' >&2
				exit 1
			}
			VM_MEM=$OPTARG
			;;
		(p)
			VM_DSKPOOL=$OPTARG
			;;
		(t)
			TFTPBOOT_DIR=$OPTARG
			;;
		('?')  # others
			usage
			exit 2
			;;
	esac
done
unset _opt

# Check parameters
test -n "${vm_name-}" || {
	echo 'No VM name provided.' >&2
	exit 1
}

test -d "${TFTPBOOT_DIR}" || {
	printf 'Cannot find tftpboot directory.\n' >&2
	printf '%s: No such file or directory\n' "${TFTPBOOT_DIR}" >&2
	exit 1
}

_cleanup() {
	if test -n "${install_net_name-}" && virsh net-info "${install_net_name}" >/dev/null 2>&1
	then
		virsh net-destroy --network "${install_net_name}"
	fi
}
trap _cleanup EXIT

# Generate installation parameters
uuid=$(cat /proc/sys/kernel/random/uuid)
install_net_name=$(printf '_install_%s' "${uuid}")
vm_mac=$(genmac)

# Create transient install network
_net_defn=$(mktemp)
cat <<EOF >"${_net_defn}"
<network>
  <name>${install_net_name}</name>
  <uuid>${uuid}</uuid>
  <forward mode='nat'/>
  <bridge stp='off' delay='0'/>
  <ip address='192.168.122.1' netmask='255.255.255.0'>
    <tftp root='${TFTPBOOT_DIR}'/>
    <dhcp>
      <range start='192.168.122.100' end='192.168.122.110'/>
      <host mac='${vm_mac}' name='${vm_name}' ip='192.168.122.100'/>
      <bootp file='${TFTP_FILE}'/>
    </dhcp>
  </ip>
</network>
EOF
virsh net-create --file "${_net_defn}"
rm "${_net_defn}"

# Install VM
virt-install --virt-type qemu --hvm \
	--name "${vm_name}" \
	--vcpus "${VM_NTHREADS},$(get_cpu_topology)" \
	--memory "${VM_MEM}" \
	--pxe \
	--disk "size=${VM_DSKSZ}$(test -n "${VM_DSKPOOL}" && printf ',pool=%s' "${VM_DSKPOOL}")" \
	--network network="${install_net_name}" \
	--graphics none \
	--rng /dev/random
