PREFIX = /usr/local

BINDIR = $(PREFIX)/bin
DATAROOTDIR = $(PREFIX)/share
DATADIR = $(DATAROOTDIR)/makevirt

INSTALL = install
RM = rm -f

.PHONY:

all: makevirt

makevirt: makevirt.in
	sed -e '/^# Defaults/,/^$$/s|^TEMPLATE_DIR=.*$$|TEMPLATE_DIR=$(DATADIR)/templates|' makevirt.in >$@

install: makevirt .PHONY
	$(INSTALL) -d '$(DESTDIR)$(BINDIR)'
	$(INSTALL) -d '$(DESTDIR)$(DATADIR)/templates'
	$(INSTALL) -m 0755 ./makevirt '$(DESTDIR)$(BINDIR)'
	$(INSTALL) -m 0644 ./templates/makevirt.*.sh '$(DESTDIR)$(DATADIR)/templates'

clean:
	$(RM) ./makevirt

uninstall: .PHONY
	$(RM) '$(DESTDIR)$(BINDIR)/makevirt'
	$(RM) -R '$(DESTDIR)$(DATADIR)'
