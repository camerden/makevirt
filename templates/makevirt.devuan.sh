# -*- mode: sh; indent-tabs-mode: t -*-

EXTRA_ARGS='lowmem/low=true --- net.ifnames=0'

devuan_help() {
	cat <<-EOF
	  -f	preseed file
	  -h	hostname
	  -s	suite (default: stable)
	EOF
}

devuan_opts() {
	while getopts 'f:h:s:' _opt
	do
		case ${_opt}
		in
			(f)
				devuan_preseed_file=${OPTARG}
				;;
			(h)
				devuan_hostname=${OPTARG}
				;;
			(s)
				devuan_suite=${OPTARG}
				;;
			('?')
				exit 1
				;;
		esac
	done
	unset _opt OPTARG
}

devuan_prepare() {
	: "${devuan_suite:=stable}"  # default to stable

	uname_m=$(uname -m)
	case ${uname_m}
	in
		(arm|armv?l)
			DEVUAN_URL_LOCATION=https://pkgmaster.devuan.org/devuan/dists/${devuan_suite}/main/installer-armhf/
			;;
		(aarch64)
			DEVUAN_URL_LOCATION=https://pkgmaster.devuan.org/devuan/dists/${devuan_suite}/main/installer-arm64/
			;;
		(ppc64le)
			DEVUAN_URL_LOCATION=https://pkgmaster.devuan.org/devuan/dists/${devuan_suite}/main/installer-ppc64el/
			;;
		(i386)
			DEVUAN_URL_LOCATION=https://pkgmaster.devuan.org/devuan/dists/${devuan_suite}/main/installer-i386/
			EXTRA_ARGS="${EXTRA_ARGS} console=ttyS0"
			;;
		(x86_64)
			DEVUAN_URL_LOCATION=https://pkgmaster.devuan.org/devuan/dists/${devuan_suite}/main/installer-amd64/
			EXTRA_ARGS="${EXTRA_ARGS} console=ttyS0"
			;;
		(*)
			printf 'Unknown machine type: %s\n' "${uname_m}" >&2
			printf 'Please supply a fix to makevirt.\n' >&2
			exit 1
	esac
	unset uname_m
}

# devuan_prepare_preseed() (
# 	test -r "$1" || {
# 		printf 'Cannot read preseed file at "%s"\n' "$1" >&2
# 		return 1
# 	}

# 	preseed_tmp=$(mktemp "${TMPDIR:-/tmp}"/preseed_XXXXXX.cfg)

# 	echo "${preseed_tmp}"
# )

devuan_virt_install() {
	devuan_prepare  # sets required variables


	set -- "$@" --location "${DEVUAN_URL_LOCATION}"

	if test -n "${devuan_preseed_file-}"
	then
		# tmp_preseed=$(devuan_prepare_preseed "${devuan_preseed_file}")
		set -- "$@" --initrd-inject "${devuan_preseed_file}"
		PRESEED_EXTRA_ARGS="file=/$(basename "${devuan_preseed_file}")"
		PRESEED_EXTRA_ARGS="${PRESEED_EXTRA_ARGS} language=en country=CH locale=en_GB.UTF-8"

		if test -n "${devuan_hostname-}"
		then
			if echo "${devuan_hostname}" | grep -q -F .
			then
				# Split up FQDN into hostname and domain
				devuan_domain=${devuan_hostname#*.}
				devuan_hostname=${devuan_hostname%%.*}
			else
				unset devuan_domain
			fi
			test -z "$(echo "${devuan_hostname-}" | tr -d 'a-z0-9-_')" \
				|| die 'Hostname must only contain [a-z0-9_-]'
			PRESEED_EXTRA_ARGS="${PRESEED_EXTRA_ARGS} hostname=${devuan_hostname:-unassigned-hostname}"

			test -z "$(echo "${devuan_domain-}" | tr -d 'a-z0-9-._')" \
				|| die 'Domain must only contain dot-separated components containing only [a-z0-9_-]'
			PRESEED_EXTRA_ARGS="${PRESEED_EXTRA_ARGS} domain=${devuan_domain:-unassigned-domain}"
		fi

		EXTRA_ARGS="${PRESEED_EXTRA_ARGS} ${EXTRA_ARGS}"
		unset PRESEED_EXTRA_ARGS
	fi

	if test -n "${EXTRA_ARGS-}"
	then
		set -- "$@" --extra-args "${EXTRA_ARGS}"
	fi

	virt_install --os-variant debiantesting "$@"
}

devuan_cleanup() {
	# if test -n "${devuan_tmp_preseed-}" && test -f "${devuan_tmp_preseed}"
	# then
	# 	rm -f "${devuan_tmp_preseed}"
	# fi

	:
}

export TEMPLATE_OPTS=devuan_opts
export TEMPLATE_HELP=devuan_help
export TEMPLATE_CLEANUP=devuan_cleanup
export VIRT_INSTALL_COMMAND=devuan_virt_install
